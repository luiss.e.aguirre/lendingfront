import { ActionTypes } from "../constants/action-types";
import axios from 'axios';
import configData from '../../settings.json';

const API_URL = configData.API_URL;

// Add Loan Application
export const addLoanApplication = loanApplication => {
  return dispatch => {
    return axios.post(`${API_URL}/loanApplication`,
      loanApplication,
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        }
      }).then(res => {
        dispatch({
          type: ActionTypes.ADD_LOAN_APPLICATION,
          payload: res.data
        });
      }).catch(err => {
        dispatch({
          type: ActionTypes.ADD_LOAN_APPLICATION,
          payload: { 'error': err }
        })
      });
  };
}
