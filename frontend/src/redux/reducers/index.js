import { combineReducers } from 'redux';
import { loanApplicationReducer } from './loanApplicationReducer';

const reducers = combineReducers({
  loanApplications: loanApplicationReducer
})

export default reducers;