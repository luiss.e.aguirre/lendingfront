import { ActionTypes } from '../constants/action-types';

const initialState = {
  state: 'New'
}
export const loanApplicationReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.ADD_LOAN_APPLICATION:
      return {
        ...state,
        ...action.payload
      };

    default:
      return state;
  }
}