import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Navbar } from './components/Navbar';
import { LoanApplicationForm } from './components/LoanApplicationForm';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <div className='container'>
        <Switch>
          <Route path='/' component={LoanApplicationForm} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
