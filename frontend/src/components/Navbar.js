import React from 'react';
import { Link } from 'react-router-dom';

export const Navbar = () => (
  <nav className='navbar navbar-expand-lg navbar-light bg-light'>
    <div className='container-fluid'>
      <Link className='navbar-brand' to='/'>Lending Front</Link>
      <button className='navbar-toggler' type='button' data-bs-toggle='collapse'
        data-bs-target='#navbarSupportedContent'
        aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
        <span className='navbar-toggler-icon' />
      </button>
    </div>
  </nav>
)