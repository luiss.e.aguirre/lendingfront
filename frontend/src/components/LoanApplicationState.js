import React from 'react';

export const LoanApplicationState = ({ state }) => {
  const backgrounColor = state === 'Approved' ? 'green' : 'red';
  return (
    <div>
      {state !== 'New' &&
        <div className={`ui right ribbon left icon label ${backgrounColor}`}>
          <h5 style={{ color: 'white' }}>{state}</h5>
        </div>
      }
    </div>
  )
}