import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addLoanApplication } from '../redux/actions/loanApplicationActions';
import { LoanApplicationState } from './LoanApplicationState';


export const LoanApplicationForm = () => {

  const [ loanApplication, setLoanApplication ] = useState({
    name: '',
    taxId: '',
    businessName: '',
    amount: 0
  });
  const [ error, setError ] = useState(false);
  const dispatch = useDispatch();
  const { state } = useSelector(state => state.loanApplications);

  const onChangeField = (event) => {
    event.preventDefault();
    setLoanApplication({
      ...loanApplication,
      [event.target.name]: event.target.value
    });
  }

  const validateFields = (fieldsNames) => {
    const fields = fieldsNames.map(field => loanApplication[field])
    return !fields.includes('');
  }

  const submitHandle = (event) => {
    event.preventDefault();
    if (! validateFields(['name', 'taxId', 'businessName'])) {
      setError(true);
      return;
    }
    setError(false);
    dispatch(addLoanApplication({
      ...loanApplication,
      amount: Number(loanApplication.amount)
    }));
  }

  return (
    <div>
      <LoanApplicationState state={state} />
      <form className={`ui form ${error ? 'error' : ''}`}>
        <div className='ui blue message'>
          <div className='header'>Please fill your information to apply for a loan</div>
        </div>
        <div className='required field'>
          <label>Name</label>
          <input type='text' name='name' placeholder='Name' onChange={onChangeField} />
        </div>
        <div className='required field'>
          <label>Tax ID</label>
          <input type='text' name='taxId' placeholder='Tax ID' onChange={onChangeField} />
        </div>
        <div className='required field'>
          <label>Business Name</label>
          <input type='text' name='businessName' placeholder='Business Name' onChange={onChangeField} />
        </div>
        <div className='field'>
          <label>Requested Amount</label>
          <input type='number' name='amount' placeholder='Requested Amount' onChange={onChangeField} />
        </div>
        { error &&
        <div className='ui error message'>
          <div className='header'>Fill all information</div>
          <p>Please fill all required fields.</p>
        </div> }
        <button className='ui button' type='submit' onClick={submitHandle}>Apply Loan</button>
      </form>
    </div>
  );
};