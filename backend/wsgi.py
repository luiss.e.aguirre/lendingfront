import sys
from os.path import dirname

filePath = dirname(__file__)
sys.path.append(dirname(__file__) + '/src')
print('FILEPATH '+filePath)

from core import app

if __name__ == "__main__":
    app.run(debug=environ.get('DEBUG'))
