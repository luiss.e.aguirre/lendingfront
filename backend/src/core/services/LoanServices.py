class LoanService:

    @staticmethod
    def approveLoan(amount):
        if amount > 50000:
            return 'Declined'
        elif amount < 50000:
            return 'Approved'
        else:
            return 'Undecided'
