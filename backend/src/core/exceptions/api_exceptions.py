from flask import jsonify
from core import app


class ApiException(Exception):
    '''All custom API exceptions'''
    def __init__(self):
      Exception.__init__(self)

class InvalidRequest(ApiException):
    code = 400
    description = 'Invalid body params'

    def __init__(self, message):
        ApiException.__init__(self)
        self.message = message

    def to_dict(self):
        return dict({
          'code': self.code,
          'description': self.description,
          'message': self.message
        })


@app.errorhandler(InvalidRequest)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    return response, error.code
