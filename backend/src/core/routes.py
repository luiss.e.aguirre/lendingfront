from core import app
from flask import request
from flask import jsonify
import sys
from .services.LoanServices import LoanService
from flask_cors import cross_origin
from .exceptions.api_exceptions import InvalidRequest


@app.route('/loanApplication', methods=['POST'])
@cross_origin()
def loanApplication():
    data = request.json
    amount = data.get('amount')
    if amount is None or amount <= 0:
      raise InvalidRequest('Amount cannot be null, zero or negative')
    state = LoanService.approveLoan(amount)
    response = jsonify({
        'state': state,
    })
    return response
