from flask import Flask
from flask_cors import CORS
from os import environ

app = Flask(__name__)
cors = CORS(app, resources={r'/loanApplication': {'origins': environ.get('FRONTEND_URL')}})
app.config['CORS_HEADERS'] = 'Content-Type'

from . import routes
