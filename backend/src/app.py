import sys
from os.path import dirname
from os import environ

filePath = dirname(__file__)
sys.path.append(dirname(__file__))

from core import app

if __name__ == "__main__":
    app.run(debug=environ.get('DEBUG'))
