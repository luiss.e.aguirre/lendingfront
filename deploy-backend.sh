echo "Deploying Backend..."
cd backend
sudo docker login -u AWS -p $(aws ecr get-login-password --region us-east-1) 425412049967.dkr.ecr.us-east-1.amazonaws.com/lending.front.backend:latest
sudo docker build . -t lending.front.backend
sudo docker tag lending.front.backend:latest 425412049967.dkr.ecr.us-east-1.amazonaws.com/lending.front.backend:latest
sudo docker push 425412049967.dkr.ecr.us-east-1.amazonaws.com/lending.front.backend:latest
cd aws_deploy
eb setenv FRONTEND_URL=http://dmr3ocgtz0xj8.cloudfront.net
eb setenv DEBUG=True
eb deploy
